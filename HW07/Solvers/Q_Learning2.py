# Licensing Information:  You are free to use or extend this codebase for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide the following
# attribution:
# This CSCE-689 RL assignment codebase was developed at Texas A&M University.
# The core code base was developed by Guni Sharon (guni@tamu.edu) based on
# code by by Denny Britz (repository: https://github.com/dennybritz/reinforcement-learning)

from collections import defaultdict
import numpy as np
import sklearn.pipeline
import sklearn.preprocessing
from sklearn.kernel_approximation import RBFSampler
from sklearn.linear_model import SGDRegressor
from Solvers.Abstract_Solver import AbstractSolver
from lib import plotting


class QLearning(AbstractSolver):
    def __init__(self, env, options):
        assert str(env.action_space).startswith("Discrete") or str(
            env.action_space
        ).startswith("Tuple(Discrete"), (
            str(self) + " cannot handle non-discrete action spaces"
        )
        assert (
            options.solvermode == "deterministic" or options.solvermode == "epsilon"
        ), (str(self) + " needs solvermode of either 'deterministic' or 'epsilon'")
        super().__init__(env, options)
        # The final action-value function.
        # A nested dictionary that maps state -> (action -> action-value).
        self.Q = defaultdict(lambda: np.zeros(env.action_space.n))

    def train_episode(self):
        """
        Run a single episode of the Q-Learning algorithm: ABWorld.

        Use:
            self.env: ABWordEnv
            self.options.steps: steps per episode
            self.epsilon_greedy_action(state): returns an epsilon greedy action
            self.epsilon_greedy_action(state): returns an epsilon greedy action
            np.argmax(self.Q[next_state]): action with highest q value
            self.options.gamma: Gamma discount factor.
            self.Q[state][action]: q value for ('state', 'action')
            self.options.alpha: TD learning rate.
            self.options.solvermode: Solver Mode for greedy policy - 'deterministic' or 'epsilon'
            next_state, reward, done, _ = self.step(action): advance one step in the environment
        """

        # Reset the environment
        state = self.env.reset()
        print("Start state: ", state)
        policy = (
            self.deterministic_greedy_action
            if self.options.solvermode == "deterministic"
            else self.epsilon_greedy_action
        )

        for _ in range(self.options.steps):
            # Action based upon solvermode
            action = policy(state)

            # Take action and observe the next state and reward
            next_state, reward, done, _ = self.step(action)

            # Update the Q-value
            self.Q[state][action] += self.options.alpha * (
                reward
                + self.options.gamma * np.max(self.Q[next_state])
                - self.Q[state][action]
            )
            state = next_state
            if done:
                break

    def __str__(self):
        return "Q-Learning2"

    def print_action_values(self):
        """
        Prints the action values for all states.
        """
        print("State\tStay\tMove")
        for state in range(self.env.nS):
            print(
                f"{state}\t{self.Q[state][self.env.stay_action]:.2f}\t{self.Q[state][self.env.move_action]:.2f}"
            )

    def create_greedy_policy(self):
        """
        Creates a greedy policy based on Q values.

        Returns:
            A function that takes an observation as input and returns a greedy
            action.
        """
        return lambda state: self.deterministic_greedy_action(state)

    def deterministic_greedy_action(self, state):
        action_values = self.Q[state]
        if np.allclose(action_values, np.tile(action_values[0], action_values.shape)):
            # Prefer move action if there is a tie.
            selected_action = self.env.move_action
        else:
            # Select best action based on Q values.
            selected_action = np.argmax(action_values)
        return selected_action

    def epsilon_greedy_action(self, state):
        """
        Return an epsilon-greedy action based on the current Q-values and
        epsilon.

        Use:
            self.env.action_space.n: size of the action space
            np.argmax(self.Q[state]): action with highest q value

        Returns:
            An action selected where epsilon refers to the probability of
            choosing to explore, and consequently exploits the highest Q-value
            with probability of (1-epsilon).
        """
        if np.random.rand() < self.options.epsilon:
            selected_action = np.random.randint(self.env.action_space.n)
        else:
            action_values = self.Q[state]
            if np.allclose(
                action_values, np.tile(action_values[0], action_values.shape)
            ):
                # Breaking ties arbitrarily
                selected_action = np.random.randint(self.env.action_space.n)
            else:
                # Select best action based on Q values.
                selected_action = np.argmax(action_values)
        return selected_action


class Estimator:
    """
    Value Function approximator. Don't change!
    """

    def __init__(self, env):
        # Feature Preprocessing: Normalize to zero mean and unit variance
        # We use a few samples from the observation space to do this
        observation_examples = np.array(
            [env.observation_space.sample() for x in range(10000)]
        )
        self.scaler = sklearn.preprocessing.StandardScaler()
        self.scaler.fit(observation_examples)

        # Used to convert a state to a featurizes represenation.
        # We use RBF kernels with different variances to cover different parts of the space
        self.featurizer = sklearn.pipeline.FeatureUnion(
            [
                ("rbf1", RBFSampler(gamma=5.0, n_components=100)),
                ("rbf2", RBFSampler(gamma=2.0, n_components=100)),
                ("rbf3", RBFSampler(gamma=1.0, n_components=100)),
                ("rbf4", RBFSampler(gamma=0.5, n_components=100)),
            ]
        )
        self.featurizer.fit(self.scaler.transform(observation_examples))
        # We create a separate model for each action in the environment's
        # action space. Alternatively we could somehow encode the action
        # into the features, but this way it's easier to code up.
        self.models = []
        for _ in range(env.action_space.n):
            model = SGDRegressor(learning_rate="constant")
            # We need to call partial_fit once to initialize the model
            # or we get a NotFittedError when trying to make a prediction
            # This is quite hacky.
            model.partial_fit([self.featurize_state(env.reset())], [0])
            self.models.append(model)

    def featurize_state(self, state):
        """
        Returns the featurized representation for a state.
        """
        scaled = self.scaler.transform([state])
        featurized = self.featurizer.transform(scaled)
        return featurized[0]

    def predict(self, s, a=None):
        """
        Makes value function predictions.

        Args:
            s: state to make a prediction for
            a: (Optional) action to make a prediction for

        Returns
            If an action a is given this returns a single number as the prediction.
            If no action is given this returns a vector or predictions for all actions
            in the environment where pred[i] is the prediction for action i.

        """
        features = self.featurize_state(s)
        if a is None:
            return np.array([m.predict([features])[0] for m in self.models])
        else:
            return self.models[a].predict([features])[0]

    def update(self, s, a, y):
        """
        Updates the estimator parameters for a given state and action towards
        the target y.
        """
        features = self.featurize_state(s)
        self.models[a].partial_fit([features], [y])
