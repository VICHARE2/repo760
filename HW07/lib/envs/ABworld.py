import io
import numpy as np
import sys
from gym.envs.toy_text import discrete

STAY = 0
MOVE = 1


class ABWorldEnv(discrete.DiscreteEnv):
    """
    This is a simple environment with 2 states and 2 actions.
    """

    metadata = {"render.modes": ["human", "ansi"]}
    name = "ABWorld"

    def __init__(self):
        nS = 2
        nA = 2

        P = {}
        for s in range(nS):
            # P[s][a] = (prob, next_state, reward, is_done)
            P[s] = {a: [] for a in range(nA)}
            P[s][STAY] = [(1.0, s, 1.0, False)]
            P[s][MOVE] = [(1.0, 1 - s, 0.0, False)]

        # We always start in state 0 (A)
        isd = np.array([1, 0], dtype=np.float64)

        # Store the actions for preferences in learning method
        self.stay_action = STAY
        self.move_action = MOVE

        super(ABWorldEnv, self).__init__(nS, nA, P, isd)

    def render(self, mode="human", close=False):
        self._render(mode, close)

    def _render(self, mode="human", close=False):
        """
        Nothing to render in this static AB environment.
        """
        return
